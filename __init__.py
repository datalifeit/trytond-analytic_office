# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import sale
from . import purchase
from . import office
from . import party


def register():
    Pool.register(
        account.Rule,
        account.AnalyticAccount,
        office.Office,
        office.OfficeAnalytic,
        module='analytic_office', type_='model')
    Pool.register(
        sale.Sale,
        sale.SaleLine,
        module='analytic_office', type_='model',
        depends=['sale_office', 'analytic_sale', 'analytic_apply_rule'])
    Pool.register(
        purchase.Purchase,
        purchase.PurchaseLine,
        module='analytic_office', type_='model',
        depends=['purchase_office', 'analytic_purchase', 'analytic_apply_rule']
    )
    Pool.register(
        party.Party,
        module='analytic_office', type_='model',
        depends=['analytic_party'])
    Pool.register(
        office.SaleLine,
        module='analytic_office', type_='model',
        depends=['sale_office', 'analytic_sale', 'analytic_party'])
    Pool.register(
        office.PurchaseLine,
        module='analytic_office', type_='model',
        depends=['purchase_office', 'analytic_purchase', 'analytic_party']
    )
    Pool.register(
        office.InvoiceLine,
        module='analytic_office', type_='model',
        depends=['account_office', 'analytic_invoice', 'analytic_party']
    )

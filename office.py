# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import PoolMeta
from trytond.model import fields, ModelSQL
from trytond.pyson import If, Eval
from trytond.transaction import Transaction


class Office(metaclass=PoolMeta):
    __name__ = 'company.office'

    analytic_accounts = fields.Many2Many(
        'company.office-analytic_account.account', 'office', 'account',
        'Analytic Accounts',
        domain=[
            ('type', '=', 'view'),
            If(Eval('company', None),
                ('company', '=', Eval('company')),
                ('company', '=', Eval('context', {}).get('company', -1))),
        ],
        depends=['company'])


class OfficeAnalytic(ModelSQL):
    """Office - Analytic Account"""
    __name__ = 'company.office-analytic_account.account'

    office = fields.Many2One('company.office', 'Office', select=True,
        ondelete='CASCADE')
    account = fields.Many2One('analytic_account.account', 'Account',
        select=True, ondelete='CASCADE')


class SaleLine(metaclass=PoolMeta):
    __name__ = 'sale.line'

    @fields.depends('sale', '_parent_sale.office')
    def get_party_analytic_account_domain(self, root):
        domain = super().get_party_analytic_account_domain(root)
        if self.sale and self.sale.office:
            domain.append(('parent.offices', '=', self.sale.office.id))
        else:
            domain.append(('parent.offices', '=',
                Transaction().context.get('office', -1)))
        return domain


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('purchase', '_parent_purchase.office')
    def get_party_analytic_account_domain(self, root):
        domain = super().get_party_analytic_account_domain(root)
        if self.purchase and self.purchase.office:
            domain.append(('parent.offices', '=', self.purchase.office.id))
        else:
            domain.append(('parent.offices', '=',
                Transaction().context.get('office', -1)))
        return domain


class InvoiceLine(metaclass=PoolMeta):
    __name__ = 'account.invoice.line'

    @fields.depends('invoice', '_parent_invoice.office')
    def get_party_analytic_account_domain(self, root):
        domain = super().get_party_analytic_account_domain(root)
        if self.invoice and self.invoice.office:
            domain.append(('parent.offices', '=', self.invoice.office.id))
        else:
            domain.append(('parent.offices', '=',
                Transaction().context.get('office', -1)))
        return domain

========================
Analytic Office Scenario
========================

Imports::

    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company


Install analytic_office::

    >>> config = activate_modules(['analytic_office', 'analytic_party'])


Create company::

    >>> _ = create_company()
    >>> company = get_company()


Get user::

    >>> User = Model.get('res.user')
    >>> admin = User(config.user)


Create party::

    >>> Party = Model.get('party.party')
    >>> party = Party(name='Party')
    >>> party.save()


Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount()
    >>> root.name = 'Root'
    >>> root.code = '1'
    >>> root.type = 'root'
    >>> root.save()
    >>> view_account = AnalyticAccount()
    >>> view_account.name = 'View 1'
    >>> view_account.code = '11'
    >>> view_account.type = 'view'
    >>> view_account.root = root
    >>> view_account.parent = root
    >>> view_account.save()
    >>> analytic_account = AnalyticAccount()
    >>> analytic_account.name = 'Analytic 1'
    >>> analytic_account.code = '111'
    >>> analytic_account.root = root
    >>> analytic_account.parent = view_account
    >>> analytic_account.save()
    >>> view_account2 = AnalyticAccount()
    >>> view_account2.name = 'View 2'
    >>> view_account2.code = '12'
    >>> view_account2.type = 'view'
    >>> view_account2.root = root
    >>> view_account2.parent = root
    >>> view_account2.save()
    >>> analytic_account2 = AnalyticAccount()
    >>> analytic_account2.name = 'Analytic 2'
    >>> analytic_account2.code = '121'
    >>> analytic_account2.root = root
    >>> analytic_account2.parent = view_account2
    >>> analytic_account2.save()


Create offices::

    >>> Office = Model.get('company.office')
    >>> office = Office()
    >>> office.name = 'Office 1'
    >>> office.company = company
    >>> office.save()
    >>> office2 = Office()
    >>> office2.name = 'Office 2'
    >>> office2.company = company
    >>> office2.save()


Add office to admin user and reload context::

    >>> admin.offices.append(office)
    >>> office = Office(office.id)
    >>> admin.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> admin.office = office
    >>> admin.save()
    >>> config._context = User.get_preferences(True, config.context)


Add analytic account to party when account office isn't set::

    >>> party.analytic_accounts.append(analytic_account)
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Analytic Accounts" in "Party" is not valid according to its domain. - 
    >>> party.reload()


Add analytic_account to offices::

    >>> office.analytic_accounts.append(analytic_account)
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> office.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Analytic Accounts" in "Company branch office" is not valid according to its domain. - 
    >>> office.reload()
    >>> office.analytic_accounts.append(view_account)
    >>> view_account = AnalyticAccount(view_account.id)
    >>> office.save()
    >>> office2.analytic_accounts.append(view_account2)
    >>> view_account2 = AnalyticAccount(view_account2.id)
    >>> office2.save()


Add analytic account Again to party::

    >>> party.analytic_accounts.append(analytic_account)
    >>> analytic_account = AnalyticAccount(analytic_account.id)
    >>> party.save()


Add wrong office to party::

    >>> party.offices.append(office2)
    >>> office2 = Office(office2.id)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Analytic Accounts" in "Party" is not valid according to its domain. - 

    >>> party.reload()


Add office to party::

    >>> party.offices.append(office)
    >>> office = Office(office.id)
    >>> party.save()


Add wrong office2 analytic account to party::

    >>> party.analytic_accounts.append(analytic_account2)
    >>> analytic_account2 = AnalyticAccount(analytic_account2.id)
    >>> party.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelstorage.DomainValidationError: The value for field "Analytic Accounts" in "Party" is not valid according to its domain. - 
    >>> party.reload()

================================
Analytic Office Rule Scenario
================================

Imports::

    >>> import datetime
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from operator import attrgetter
    >>> from proteus import Model, Wizard, Report
    >>> from trytond.tests.tools import activate_modules, set_user
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences, create_payment_term
    >>> today = datetime.date.today()

Install analytic_office::

    >>> config = activate_modules(['analytic_sale', 'sale_office', 'analytic_office', 'analytic_apply_rule'])

Create company::

    >>> Company = Model.get('company.company')
    >>> _ = create_company()
    >>> company = get_company()

Create sale user::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> sale_user = User()
    >>> sale_user.name = 'Sale'
    >>> sale_user.login = 'sale'
    >>> sale_user.companies.append(company)
    >>> sale_user.company = Company(company.id)
    >>> sale_group, = Group.find([('name', '=', 'Sales')])
    >>> sale_user.groups.append(sale_group)
    >>> sale_user.save()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create branch office::

    >>> Office = Model.get('company.office')
    >>> office1 = Office(name='Office 1')
    >>> office1.save()
    >>> office2 = Office(name='Office 2')
    >>> office2.save()
    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> user = User(config.user)
    >>> user.offices.extend([office1, office2])
    >>> office1 = Office(office1.id)
    >>> user.office = office1
    >>> user.save()
    >>> set_user(user)
    >>> config._context = User.get_preferences(True, {})

Create analytic accounts::

    >>> AnalyticAccount = Model.get('analytic_account.account')
    >>> root = AnalyticAccount(type='root', name='Root')
    >>> root.save()
    >>> analytic_account = AnalyticAccount(root=root, parent=root,
    ...     name='Analytic')
    >>> analytic_account.save()
    >>> mandatory_root = AnalyticAccount(type='root', name='Root')
    >>> mandatory_root.save()
    >>> mandatory_analytic_account1 = AnalyticAccount(root=mandatory_root,
    ...     parent=mandatory_root, name='Mandatory Analytic 1')
    >>> mandatory_analytic_account1.save()
    >>> mandatory_analytic_account2 = AnalyticAccount(root=mandatory_root,
    ...     parent=mandatory_root, name='Mandatory Analytic 2')
    >>> mandatory_analytic_account2.save()
    >>> mandatory_analytic_account3 = AnalyticAccount(root=mandatory_root,
    ...     parent=mandatory_root, name='Mandatory Analytic 3')
    >>> mandatory_analytic_account3.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> customer = Party(name='Customer')
    >>> customer.save()
    >>> customer2 = Party(name='Customer 2')
    >>> customer2.save()

Create analytic rules::

    >>> Rule = Model.get('analytic_account.rule')
    >>> rule = Rule()
    >>> _, mandatory_entry = rule.analytic_accounts
    >>> mandatory_entry.root = mandatory_root
    >>> mandatory_entry.account = mandatory_analytic_account3
    >>> rule.save()
    >>> rule = Rule(sale=True, party=customer, office=office2)
    >>> _, mandatory_entry = rule.analytic_accounts
    >>> mandatory_entry.root = mandatory_root
    >>> mandatory_entry.account = mandatory_analytic_account1
    >>> rule.save()
    >>> rule = Rule(sale=True, office=office1)
    >>> _, mandatory_entry = rule.analytic_accounts
    >>> mandatory_entry.root = mandatory_root
    >>> mandatory_entry.account = mandatory_analytic_account2
    >>> rule.save()

Create account category::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'product'
    >>> template.default_uom = unit
    >>> template.type = 'goods'
    >>> template.salable = True
    >>> template.list_price = Decimal('10')
    >>> template.account_category = account_category
    >>> template.save()
    >>> product, = template.products

Create payment term::

    >>> payment_term = create_payment_term()
    >>> payment_term.save()

Sale with analytic accounts::

    >>> Sale = Model.get('sale.sale')
    >>> SaleLine = Model.get('sale.line')
    >>> sale = Sale()
    >>> sale.party = customer
    >>> sale.payment_term = payment_term
    >>> sale.office == office1
    True
    >>> sale_line = sale.lines.new()
    >>> mandatory_entry, = [a for a in sale_line.analytic_accounts if a.account == mandatory_analytic_account2]